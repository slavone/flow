class Loan
  attr_accessor :id, :debt, :account_id

  def initialize(id:, debt:, account_id:)
    @id = id
    @debt = debt
    @account_id = account_id
  end
end
