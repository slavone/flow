class TransactionsPerformer < BaseService
  attr_reader :transaction_id, :loan, :balance, :transaction_result

  def initialize(loan_id)
    @loan = DB.find loan_id
  end

  def call
    # TODO: add per transaction uuid to search logs
    @balance = BalanceChecker.call(@loan.account_id)
    return false unless balance_positive?

    @transaction_id = TransactionsFetcher.call(@loan)
    return false unless transaction_id

    @transaction_result = TransactionsRunner.call(transaction_id)

    return false unless transaction_result
    logger.info "Transaction #{transaction_id} was #{transaction_result}"
    transaction_result
  end

  private

  def balance_positive?
    balance && balance > 0
  end
end
