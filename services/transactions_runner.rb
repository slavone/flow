class TransactionsRunner < BaseService
  class Unavailable < StandardError; end

  attr_reader :transaction_id

  def initialize(transaction_id)
    @transaction_id = transaction_id
  end

  def call
    with_retries do
      RunTransaction.new(transaction_id).call
    end
  end
end
