class TransactionsFetcher < BaseService
  class Unavailable < StandardError; end

  attr_reader :transaction_id

  def initialize(loan)
    @loan = loan
  end

  def call
    @transaction_id = with_retries do
      FetchTransactions.new(@loan.id).call
    end
    logger.info "Fetched transaction #{transaction_id} for loan #{@loan.id}"
    transaction_id
  end
end
