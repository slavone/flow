class BalanceChecker < BaseService
  class Unavailable < StandardError; end

  attr_reader :account_id

  def initialize(account_id)
    @account_id = account_id
  end

  def call
    with_retries do
      CheckBalance.new(account_id).call
    end
  end
end
