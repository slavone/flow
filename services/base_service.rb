require 'logger'

class BaseService
  MAX_RETRIES = 5

  def self.call(*args)
    new(*args).call
  end

  def call
    raise NotImplementedError, 'implement :call method'
  end

  private

  def logger
    @logger ||= Logger.new(STDOUT)
  end

  def with_retries(max_retries = MAX_RETRIES)
    yield
    # TODO: if service called successfully, check the retry queue
    # and restart transactions that failed with ServiceName::Unavailable error
  rescue => e
    unless retries_left?(max_retries)
      logger.error "#{self.class} service call failed more than #{max_retries} and is probably broke."
      raise self.class::Unavailable
    end
    logger.warn "#{self} failed with #{e.inspect}"
    increment_retries
    retry
  end

  def retries_left?(max_retries)
    @retries.nil? || @retries < max_retries
  end

  def increment_retries
    @retries ||= 0
    @retries += 1
    sleep(@retries * rand(1..3))
  end
end
