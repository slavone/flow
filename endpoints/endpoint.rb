require 'date'
require 'net/http'

module Endpoint
  def call
    raise Net::HTTPBadResponse if broken?

    case rand() * 100
    when 1.0..100.0
      success
    when 0.002...1.0
      raise Timeout::Error
    else
      set_broken_until next_day
      raise Net::HTTPBadResponse
    end
  end

  def success
    raise NotImplementedError, 'implement :success callback'
  end

  private

  def next_day
    Time.now + 1 * 86400 # 24 * 60 * 60
  end

  def broken?
    broken_until && broken_until > Time.now
  end

  def broken_until
    self.class.instance_variable_get :@broken_until
  end

  def set_broken_until(val)
    self.class.instance_variable_set :@broken_until, val
  end
end
