class FetchTransactions
  include Endpoint

  def initialize(_loan_id)
    @@transaction_id ||= 0
  end

  def success
    @@transaction_id += 1
  end
end
