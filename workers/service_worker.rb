class ServiceWorker
  include Sidekiq::Worker

  sidekiq_options queue: 'remote_apis'

  def perform(service, *args)
    service_object(service).call(*args)
  end

  private

  def service_object(service)
    service.kind_of?(Class) ? service : Object.const_get(service)
  end
end
