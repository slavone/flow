FROM ruby:2.3.4-alpine3.4
ENV app /app/
COPY Gemfile* $app
WORKDIR $app
RUN apk add --update --no-cache tzdata \
  && bundle install --jobs 8 \
  && mkdir -p tmp/pids
ADD . $app
CMD ["bundle", "exec", "ruby", "scheduler.rb"]
