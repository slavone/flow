require_relative 'app'

# binding.pry

scheduler = Rufus::Scheduler.new

scheduler.every '30s' do
  DB.all.each do |loan|
    ServiceWorker.perform_async TransactionsPerformer, loan.id
  end
end

trap "INT"  do scheduler.shutdown end
trap "TERM" do scheduler.shutdown end

scheduler.join
