require 'bundler'
Bundler.require(:default)
require_relative 'endpoints/endpoint'
require_relative 'endpoints/check_balance'
require_relative 'endpoints/fetch_transactions'
require_relative 'endpoints/run_transaction'
require_relative 'models/loan'
require_relative 'workers/service_worker'
require_relative 'services/base_service'
require_relative 'services/balance_checker'
require_relative 'services/transactions_fetcher'
require_relative 'services/transactions_runner'
require_relative 'services/transactions_performer'
require_relative 'db'

sidekiq_config = { url: ENV.fetch('REDIS_URL', 'redis://localhost:6379/0') }

Sidekiq.configure_server do |config|
  config.redis = sidekiq_config
end

Sidekiq.configure_client do |config|
  config.redis = sidekiq_config
end
