class DB
  VALUES = (1..100).reduce({}) do |storage, id|
    storage[id] = rand(-9999..-1000)
    storage
  end

  class << self
    def all
      VALUES.map { |id, debt| Loan.new id: id, debt: debt, account_id: rand(10_000) }
    end

    def find(loan_id)
      debt = VALUES[loan_id]
      return nil unless debt
      Loan.new id: loan_id, debt: debt, account_id: rand(10_000)
    end
  end
end
